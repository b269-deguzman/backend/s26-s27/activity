let users = {
	"firstName": "Justine",
	"lastName": "De Guzman",
	"email" : "justinedeguzman@gmail.com",
	"password" : "pogiako123",
	"isAdmin?" : "false",
	"mobileNumber" : "09459877123"
}

let orders = {
	"userID" : "001",
	"transactionDate": "2023-03-27",
	"status" : "paid",
	"total": "5000"
}

let products = {
	"name" : "Realme earbuds",
	"description" : "It is a wireless earphone equipped with gaming mode.",
	"price": "2500",
	"stocks" : "20",
	"isActive?": "true",
	"stockKeepingUnit" : "BV011ASA"
}

let orderProducts = {
	"orderId" : "0001",
	"productId" : "01",
	"quantity": "2",
	"price" : "2500",
	"subTotal": "5000"
}